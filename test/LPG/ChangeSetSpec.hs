module LPG.ChangeSetSpec (spec) where

import LPG.Data (Expr(..), Prop(..), Reference(..), Rule(..), Value(..))
import LPG.ChangeSet (Bindings, Rewrite(..), checkBindings)
import LPG.Parser (Str, parseRule)
import LPG.Rewrite (changeSet)

import Data.Bifunctor (bimap)
import Data.List.NonEmpty (NonEmpty(..))
import Data.Either (isRight, isLeft)
import Test.Hspec

checkBindingsLHS :: Str -> Either (NonEmpty String) Bindings
checkBindingsLHS input
  = bimap (:|[]) id (parseRule input)
  >>= checkBindings (Left ()) . lhs

checkBindings' :: Str -> Either (NonEmpty String) Bindings
checkBindings' input
  = bimap (:|[]) id (parseRule input)
  >>= \Rule{lhs, rhs = Just (p:ps)} ->
        checkBindings (Left ()) lhs >>= \bs -> checkBindings (Right bs) (p:|ps)

spec :: Spec
spec = do
  describe "Check bindings (successes)"
    (do
        it "Trivial" $ checkBindingsLHS "()" `shouldSatisfy` isRight
        it "Simple reuse"
          $ checkBindingsLHS "(x:Opa)-->(x)" `shouldSatisfy` isRight
        it "Simple reuse, different patterns"
          $ checkBindingsLHS "(x:Opa) (x)-->()" `shouldSatisfy` isRight
        it "Complex reuse"
          $ checkBindingsLHS "(prev:Rule)-[m:DerivedBy]->(goal:Goal) (goal)-[r:Derives]->(ded:Formula) (ded)<-[Builds {ant: true}]-(hyp:Formula) (ded)<-[:Builds {csq: true}]-(conc:Formula)" `shouldSatisfy` isRight
        it "Property from lhs" $
          checkBindings' "(x:Opa) => (y {n: x.n})" `shouldSatisfy` isRight
    )
  describe "Check bindings (failures)"
    (do
        it "Simple reuse with label" $
          checkBindingsLHS "(x:Oi)-->(x:Bla)" `shouldSatisfy` isLeft

        it "Simple reuse with property" $
          checkBindingsLHS "(x:Oi)-->(x {n: 1})" `shouldSatisfy` isLeft

        it "Wrong order" $
          checkBindingsLHS "(x)-->(:Bla) (x:Hum)" `shouldSatisfy` isLeft

        it "Repeat edge" $
          checkBindingsLHS "()-[r:R]->() ()<-[r]-()" `shouldSatisfy` isLeft

        it "None/edge" $
          checkBindingsLHS "(x:Oi)-->(y)-[x]->()" `shouldSatisfy` isLeft

        it "Edge/node" $
          checkBindingsLHS "()-[y:Opa]->()-[x]->(y)" `shouldSatisfy` isLeft

        it "Property lookup" $
          checkBindingsLHS "(x {n: y.n})" `shouldSatisfy` isLeft

        it "Label-less unnamed node rhs" $
          checkBindings' "(x:Opa) => ()" `shouldSatisfy` isLeft

        it "Label-less unnamed edge rhs" $
          checkBindings' "(x:Opa) => (x)-->(y:Opa)" `shouldSatisfy` isLeft

        it "Labeled reuse" $
          checkBindings' "(x:Opa) => (x:Oops) (x:Oopsie)" `shouldSatisfy` isLeft
    )
  describe "Change set (failures)"
    (do
        it "variable out of scope in return" $
          changeSet "() true, 2, x.n" `shouldSatisfy` isLeft
    )
  describe "Change set (successes)"
    (do
        it "simple node deletion" $
          changeSet "(x) => " `shouldBe` (Right [(Named "x", DeleteNode:|[])])

        it "simple edge deletion" $
          changeSet "()-[r]->() => " `shouldBe` (Right [(Named "r", DeleteEdge:|[])])

        it "simple node creation" $
          changeSet "() => (x:X {n: 1})" `shouldBe` (Right [(Named "x", AddNode ["X"] [Prop ("n", V $ I 1)]:|[])])

        it "creates node" $
          changeSet "() => (x:X {n: 1}) (x)" `shouldBe` (Right [(Named "x", AddNode ["X"] [Prop ("n", V $ I 1)]:|[])])

        it "simple edge creation" $
          changeSet "(x) // ...\n (y) => (x)-[r:R]->(y)" `shouldBe` (Right [(Named "r", AddEdge "x" "y" ["R"] []:|[])])

        it "anonymous edge creation" $
          changeSet "(x) // ...\n (y) => (x)-[:R]->(y)" `shouldBe` (Right [(Anon 1, AddEdge "x" "y" ["R"] []:|[])])

        it "don't delete label if unnecessary" $
          -- should only delete labels if other labels are specified
          changeSet "(x:X) /* don't delete label*/ => (x)" `shouldBe` (Right [])

        it "deletes label if necessary" $
          changeSet "(x:X:Y) => (x:X)" `shouldBe` (Right [(Named "x", RemLabel "Y":|[])])

        it "adds label" $
          changeSet "(x:X) => (x:X:Y)" `shouldBe` (Right [(Named "x", AddLabel "Y":|[])])

        it "adds property" $
          changeSet "(x:X) => (x:X {n: 1} )" `shouldBe` (Right [(Named "x", SetProp (Prop ("n", V $ I 1)):|[])])

        it "inverts edge" $
          changeSet "(x:X)-[r]->(y:Y) => (x:X)<-[r]-(y:Y)" `shouldBe` (Right [(Named "r", ChangeEdge "y" "x" [] []:|[])])

        it "changes edge target" $
          changeSet "(x:X)-[r]->()-->(z) => (x:X)-[r]->(z)" `shouldBe` (Right [(Named "r", ChangeEdge "x" "z" [] []:|[])])

        it "changes edge source" $
          changeSet "()-[r]->(y)-->(z) => (z)-[r]->(y)" `shouldBe` (Right [(Named "r", ChangeEdge "z" "y" [] []:|[])])

    )
