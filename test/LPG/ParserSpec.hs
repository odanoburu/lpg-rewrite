module LPG.ParserSpec (spec) where

import LPG.Data ( EdgeDir(..), EdgeSpec(..), Expr(..), NodeSpec(..), Pattern
                , Prop(..), RTree(..), Rule(..), Value(..))
import LPG.Parser (Parser, patternP, rule, ruleSet, spaceConsumer)

import Data.List.NonEmpty (NonEmpty(..))
import Test.Hspec
import Test.Hspec.Megaparsec (shouldFailOn, shouldParse, shouldSucceedOn)
import Text.Megaparsec hiding (parse)
import qualified Text.Megaparsec as P

patternP' :: Parser Pattern
patternP' = patternP id

anonNode :: NodeSpec
anonNode = NodeSpec Nothing [] []

anonEdge :: EdgeDir -> EdgeSpec
anonEdge d = EdgeSpec Nothing d [] []

parse p input = P.parse (spaceConsumer *> p <* eof) "" input

spec :: Spec
spec = do
  describe "rule parsing"
    (do
        it "parses no RHS" $
          parse rule "()" `shouldParse` Rule (Leaf anonNode:|[]) Nothing []

        it "parses empty RHS" $
          parse rule " () \n=> \n "
          `shouldParse` Rule (Leaf anonNode:|[]) (Just []) []

        it "parses simple RHS" $
          parse rule " () \n=> \n (x:X)" `shouldParse` Rule (Leaf anonNode:|[]) (Just [Leaf $ NodeSpec (Just "x") ["X"] []]) []

        it "parses empty RHS with return" $
          parse rule "() 1, true" `shouldParse` Rule (Leaf anonNode:|[]) Nothing [V $ I 1, V $ B True]

        it "parses empty RHS with return list" $
          parse rule "() [1, true, \"opa\"]" `shouldParse` Rule (Leaf anonNode:|[]) Nothing [Li [V $ I 1, V $ B True, V $ T "opa"]]

        it "parses empty RHS with return map" $
          parse rule "() {n: 1, b : true, t: \"opa\"}" `shouldParse` Rule (Leaf anonNode:|[]) Nothing [M [Prop ("b", V $ B True), Prop ("n", V $ I 1), Prop ("t", V $ T "opa")]]

        it "parses simple RHS with return function call" $
          parse rule "(x) aFunc(x)" `shouldParse` Rule (Leaf (NodeSpec (Just "x") [] []):|[]) Nothing [FC "aFunc" "x"]
    )
  describe "pattern parsing"
    (do
        it "parses empty node pattern" $
          parse patternP' "/* \129300 */ ()  " `shouldParse` Leaf anonNode

        it "parses node pattern with label" $
          parse patternP' "(: Label) // um comentário " `shouldParse` Leaf (NodeSpec Nothing ["Label"] [])

        it "parses node pattern with multiple labels" $
          parse patternP' "(:Label:AnotherLabel)  " `shouldParse` Leaf (NodeSpec Nothing ["AnotherLabel", "Label"] [])

        it "parses node pattern with property" $
          parse patternP' "({n: 1})  " `shouldParse` Leaf (NodeSpec Nothing [] [Prop ("n", V $ I 1)])

        it "parses node pattern with property expression" $
          parse patternP' "({n: x.length})  " `shouldParse` Leaf (NodeSpec Nothing [] [Prop ("n", Lookup "x" "length")])

        it "parses node pattern with multiple properties" $
          parse patternP' "({ n : 1 , b:true, })  " `shouldParse` Leaf (NodeSpec Nothing [] [Prop ("b", V $ B True), Prop ("n", V $ I 1)])

        it "parses node pattern with label and property" $
          parse patternP' "(:Label:AnotherLabel {b: false, n: 33})  " `shouldParse` Leaf (NodeSpec Nothing ["AnotherLabel", "Label"] [Prop ("b", V $ B False), Prop ("n", V $ I 33)])

        it "parses named node pattern with label and property" $
          parse patternP' "(x:Label:AnotherLabel {b: false, n: 33})  " `shouldParse` Leaf (NodeSpec (Just "x") ["AnotherLabel", "Label"] [Prop ("b", V $ B False), Prop ("n", V $ I 33)])

        it "parses edge pattern (right)" $
          parse patternP' "()-->() " `shouldParse` Node anonNode (anonEdge R) (Leaf $ anonNode)

        it "parses edge pattern (left)" $
          parse patternP' "()<--() " `shouldParse` Node anonNode (anonEdge L) (Leaf $ anonNode)

        it "parses edge pattern with edge restriction" $
          parse patternP' " ()<-[:Label  {n: 12, b: true}]-()  " `shouldParse` Node anonNode (EdgeSpec Nothing L ["Label"] [Prop ("b", V $ B True), Prop ("n", V $ I 12)]) (Leaf $ anonNode)

        it "parses named edge pattern with edge restriction" $
          parse patternP' " ()<-[e:Label  {n: 12, b: true}]-()  " `shouldParse` Node anonNode (EdgeSpec (Just "e") L ["Label"] [Prop ("b", V $ B True), Prop ("n", V $ I 12)]) (Leaf $ anonNode)

        it "parses a<-b->c" $
          parse patternP' " (:A)<--()-->(:C)  " `shouldParse` Node (NodeSpec Nothing ["A"] []) (anonEdge L) (Node anonNode (anonEdge R) (Leaf $ NodeSpec Nothing ["C"] []))

        it "parses a<-b<-c" $
          parse patternP' " (:A)<--()<--(:C)  " `shouldParse` Node (NodeSpec Nothing ["A"] []) (anonEdge L) (Node anonNode (anonEdge L) (Leaf $ NodeSpec Nothing ["C"] []))

        it "parses a->b->c" $
          parse patternP' " (:A)-->()-->(:C)  " `shouldParse` Node (NodeSpec Nothing ["A"] []) (anonEdge R) (Node anonNode (anonEdge R) (Leaf $ NodeSpec Nothing ["C"] []))

        it "parses a<-b<-c->d" $
          parse patternP' " (:A)<--(:B)<--(c:C)-[r]->(:D)  " `shouldParse` Node (NodeSpec Nothing ["A"] []) (anonEdge L) (Node (NodeSpec Nothing ["B"] []) (anonEdge L) (Node (NodeSpec (Just "c") ["C"] []) (EdgeSpec (Just "r") R [] []) (Leaf $ NodeSpec Nothing ["D"] [])))

        it "parses complex specification" $
          parse rule `shouldSucceedOn` "(prev:Rule)-[m:DerivedBy]->(goal:Goal) (goal)-[d:Derives]->(ded:Formula) (andleft:Formula)-[bl:Builds {andleft: true}]->(ded)<-[br:Builds {andright:true}]-(andright:Formula)"

        it "parses rule set" $
          parse ruleSet `shouldSucceedOn` "# opa  \n  ()"

        it "parses rule sets" $
          parse ruleSet `shouldSucceedOn` "# opa  \n  () \n # incrível \n () \n \n"
    )
  describe "pattern parsing failures"
    (do
        it "fails on incomplete node" $
          parse patternP' `shouldFailOn` " (:A  "

        it "fails on erroneous edge spec" $
          parse patternP' `shouldFailOn` " (:A)->()  "

        it "fails on missing node" $
          parse patternP' `shouldFailOn` " (:A)-[r:Label]->  "

        it "fails parsing repeat labels" $
          parse patternP' `shouldFailOn` "(x:X:X)"

        it "fails parsing repeat properties" $
          parse patternP' `shouldFailOn` "(x:X {n: 1, n:2})"

        it "parses simple RHS with return function call" $
          parse rule "() aFunc(x)" `shouldParse` Rule (Leaf anonNode:|[]) Nothing [FC "aFunc" "x"]
        )
