module Main (main) where

import qualified Data.List.NonEmpty as NE
import Data.Semigroup (sconcat)
import qualified Data.Text.Lazy.IO as TLIO
import Options.Applicative

import LPG.Rewrite (changeSet, cypher, parseRule)


data Input = StdIn String | File String
data Cmd = Parse | CalcChanges | TranspileToCypher
data Arguments = Args {cmd :: Cmd, inputMode :: Input}

showHelpOnErrorExecParser :: ParserInfo a -> IO a
showHelpOnErrorExecParser = customExecParser (prefs showHelpOnError)

lpgRwProgDesc, lpgRwHeader :: String
lpgRwProgDesc = "Parse, check, and rewrite labeled-property graphs"
lpgRwHeader = "lpgRW — rewrite labeled-property graphs"


subcommands :: (Parser a -> Parser b) -> [(String, Parser a, String)] -> Parser b
subcommands f = hsubparser . foldMap raise
  where
    raise (name, p, desc) = command name (info (f p) (fullDesc <> progDesc desc))

parseSubCommand :: Parser Cmd -> Parser Arguments
parseSubCommand subcommand = Args <$> subcommand <*> (file <|> stdIn)
  where
    stdIn = StdIn <$> strOption (metavar "INPUT" <> short 'i' <> long "in"
       <> help "Read input from standard input")
    file = File <$> strOption (metavar "FILE" <> short 'f' <> long "file"
       <> help "Read input from FILE")

parseCommand :: Parser Arguments
parseCommand = subcommands parseSubCommand
  [ ("parse", pure Parse, "Parse rewrite rule")
  , ("changes", pure CalcChanges, "List changes made by rewrite")
  , ("cypher", pure TranspileToCypher, "Produce Cypher output corresponding to rewrite rule")
  ]

main :: IO ()
main = do
  args <- showHelpOnErrorExecParser
    $ info (helper <*> parseCommand)
    (fullDesc <> progDesc lpgRwProgDesc <> header lpgRwHeader)
  input <- case inputMode args of
             StdIn inp -> pure inp
             File name -> readFile name
  (case cmd args of
    Parse -> either putStrLn print . parseRule
    CalcChanges -> either (putStrLn . sconcat . NE.intersperse "\n") print
      . changeSet
    TranspileToCypher -> either (putStrLn . sconcat . NE.intersperse "\n")
                                TLIO.putStrLn . cypher
    ) input
