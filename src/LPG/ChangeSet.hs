module LPG.ChangeSet ( Bindings, ChangeSet, Rewrite(..)
                     , buildDesc, changeSet, checkBindings, references) where

import LPG.Data (Description, EdgeDir(..), EdgeSpec(..), EntityDesc(..), Expr(..)
                , Label(..), Name, NodeSpec(..), Pattern, Prop(..)
                , Reference(..), RTree(..), Rule(..), addEntityDesc, nonEmptyMap, singleton)

import Data.Bifoldable (bifoldl')
import Data.Foldable (foldl')
import Data.List.NonEmpty (NonEmpty(..))
import qualified Data.List.NonEmpty as NE
import Data.Map (Map)
import qualified Data.Map as M
import Data.Map.Merge.Strict (mapMaybeMissing, merge, zipWithMaybeMatched)
import Data.Maybe (fromMaybe, mapMaybe)
import Data.Set (Set)
import qualified Data.Set as S
--import Debug.Trace (trace)


patternHeadName :: Pattern -> Maybe Name
patternHeadName (Leaf (NodeSpec mn _ _)) = mn
patternHeadName (Node (NodeSpec mn _ _) _ _) = mn

report :: Name -> String -> String
report name msg = concat [name, ": ", msg]

data Binding = NodeB | EdgeB
  deriving (Eq, Ord, Show)

type Bindings = Map Name Binding


checkBindings :: Either () Bindings -- ^ determines check of LHS or RHS (RHS
                                    -- needs info from LHS)
  -> NonEmpty Pattern -> Either (NonEmpty String) Bindings
checkBindings side ps' =
  case foldl' go (mempty :: Bindings, []) ps' of
    (bs, []) -> Right bs
    (_, e:es) -> Left (e:|es)
  where
    go = bifoldl' node edge
      where
        node (seen, errs) (NodeSpec Nothing ls prps)
          = case (side, ls) of
              (Right{}, []) -> (seen, "Label-less unnamed node may not appear in right-hand side":errs)
              _ -> (seen, checkProps seen prps ++ errs)
        node (seen, errs) (NodeSpec (Just n) ls prps) =
          case M.insertLookupWithKey (\_ v _ -> v) n NodeB seen of
            (Nothing, seen') -> (seen', checkProps seen prps ++ errs)
            (Just EdgeB, _) -> (seen, report n "node and edge may not share the same name":errs)
            (Just NodeB, _) ->
              (seen, case (ls, prps) of
                       ([], []) -> errs
                       _ -> report n "node constraint must appear at declaration, not use":errs)
        edge (seen,errs) (EdgeSpec Nothing _ ls prps)
          = case (side, ls) of
              (Right{}, []) -> (seen, "Label-less unnamed edge may not appear in right hand side of rule":errs)
              _ -> (seen, checkProps seen prps ++ errs)
        edge (seen, errs) (EdgeSpec (Just n) _ _ prps) =
          case M.insertLookupWithKey (\_ v _ -> v) n EdgeB seen of
            (Nothing, s) -> (s, checkProps seen prps ++ errs)
            (Just NodeB, _) -> (seen, report n "edge and node may not share the same variable name":errs)
            (Just EdgeB, _) -> (seen, report n "edge may not appear twice in pattern":errs)
        checkProps seen = mapMaybe validExpr
          where
            presentInLHS var =
              -- variable is valid if only present in LHS
              case side of
                Right bs -> var `M.member` bs
                _ -> False
            validExpr (Prop (k, Lookup var _)) =
              if var `M.member` seen || presentInLHS var then Nothing
              else Just . report k $ unwords ["variable", var, "is not in scope"]
            validExpr _ = Nothing


buildDesc :: NonEmpty Pattern -> Description
buildDesc = snd . foldr go (0 :: Word, mempty)
  where
    go (Leaf (NodeSpec mname ls ps)) (n, desc) =
      let (n', ref) =
            case mname of
              Just aName -> (n, Named aName)
              Nothing   -> (n+1, Anon $ n+1)
      in (n', addEntityDesc ref (NodeDesc ls ps) desc)
    go (Node headNode (EdgeSpec mname ed ls ps) p) (n, desc) =
          let (n', desc') = go p (n, desc)
              (n'', desc'') = go (Leaf headNode) (n', desc')
              (n''', ref) =
                case mname of
                  Just aName -> (n'', Named aName)
                  Nothing -> (n''+1, Anon $ n''+1)
              -- INVARIANT: this works because the pattern headNodes are last to be
              -- added (or we'd have the wrong names/indices)
              ln = fromMaybe (show n'') $ patternHeadName (Leaf headNode)
              rn = fromMaybe (show n')  $ patternHeadName p
      in (n''', addEntityDesc ref (edgeDesc ed ln rn ls ps) desc'')
    edgeDesc R l r cs = EdgeDesc l r cs
    edgeDesc L l r cs = EdgeDesc r l cs


data Rewrite
  = AddNode [Label] [Prop]
  | AddEdge Name Name [Label] [Prop]
  | ChangeEdge Name Name [Label] [Prop] -- change source and/or target
  | AddLabel Label
  | RemLabel Label
  | SetProp Prop
  | RemProp Name
  | DeleteNode
  | DeleteEdge
  deriving (Eq, Show)

type ChangeSet = Map Reference (NonEmpty Rewrite)

changeSet :: Rule -> Either (NonEmpty String) ChangeSet
changeSet Rule{lhs, rhs, ret} =
  case bindingsOK of
    Left errs -> Left errs
    Right _ ->
      case mRhsDesc of
        Nothing -> Right mempty
        Just rhsDesc -> Right $ go lhsDesc rhsDesc
  where
    bindingsOK = do
      bs <- checkBindings (Left ()) lhs
      bs' <- case rhs of
              Just (p:ps) -> do
                bs' <- checkBindings (Right bs) (p:|ps)
                pure $ bs' <> bs
              _ -> pure bs
      case mapMaybe (validExpr bs') ret of
        [] -> pure bs'
        (e:errs) -> Left (e:|errs)
        where
          outOfScope var = Just . report var
            $ unwords ["variable", var, "is not in scope"]
          validExpr bs (Lookup var _)
            | var `M.member` bs = Nothing
            | otherwise = outOfScope var
          validExpr bs (FC _fname var)
            -- NOTE: we don't check function names for now because different
            -- backends may have different available functions
            | var `M.member` bs = Nothing
            | otherwise = outOfScope var
          validExpr bs (Li es) = foldMap (validExpr bs) es
          validExpr bs (M kvs) = foldMap (\(Prop (_k, e)) -> validExpr bs e) kvs
          validExpr _ V{} = Nothing
    lhsDesc = buildDesc lhs
    mRhsDesc = nonEmptyMap mempty buildDesc <$> rhs
    go = merge (mapMaybeMissing toDelete) (mapMaybeMissing toCreate)
               (zipWithMaybeMatched toMerge)
      where
        toDelete k NodeDesc{} = constNotAnon [DeleteNode] k
        toDelete k EdgeDesc{} = constNotAnon [DeleteEdge] k
        toCreate _ (NodeDesc ls prps) = Just . singleton $ AddNode ls prps
        toCreate _ (EdgeDesc src tgt ls prps)
          = Just . singleton $ AddEdge src tgt ls prps
        toMerge Named{} (NodeDesc ls prps) (NodeDesc ls' prps') =
          let rs = rewriteLabels ls ls' ++ rewriteProps prps prps'
          in NE.nonEmpty rs
        -- NOTE: anonymous matches are spurious
        toMerge Anon{} _ (NodeDesc ls prps) =
          Just . singleton $ AddNode ls prps
        toMerge Named{} (EdgeDesc src tgt ls prps) (EdgeDesc src' tgt' ls' prps') =
          NE.nonEmpty $
          case src == src' && tgt == tgt' of
            True -> rewriteLabels ls ls' ++ rewriteProps prps prps'
            -- because cypher has no way of redirecting edges without APOC
            _ -> [ChangeEdge src' tgt' labels (props prps prps')]
              where
                labels = case ls' of [] -> ls; _ -> ls'
                -- NOTE: must be sorted; very similar to rewriteProps, so
                -- changes to one should propagate to the other
                props [] ps' = ps'
                props _ []  = []
                props (p:ps) (p':ps') =
                  case compare p p' of
                    LT -> p : props ps (p':ps')
                    EQ -> p' : props ps ps'
                    GT -> p' : props (p:ps) ps'
        -- NOTE: anonymous matches are spurious
        toMerge Anon{} _ (EdgeDesc src tgt ls prps) =
          Just . singleton $ AddEdge src tgt ls prps
        -- errors
        toMerge _ NodeDesc{} EdgeDesc{} = Nothing
        toMerge _ EdgeDesc{} NodeDesc{} = Nothing
        constNotAnon _ Anon{} = Nothing
        constNotAnon c Named{} = NE.nonEmpty c
        -- INVARIANT: labels must be sorted (parser does it)
        --- NOTE: if no labels are specified in RHS, we don't change the labels
        --- (this avoids the user having to specify all labels all the time to
        --- keep them unchange)
        rewriteLabels _ [] = []
        rewriteLabels xs ys = goLabels xs ys
          where
            goLabels ls [] = fmap RemLabel ls
            goLabels [] ls' = fmap AddLabel ls'
            goLabels (l:ls) (l':ls') =
              case compare l l' of
                EQ -> goLabels ls ls'
                LT -> RemLabel l : goLabels ls (l':ls')
                GT -> AddLabel l' : goLabels (l:ls) ls'
        -- INVARIANT: properties must be sorted (parser does it)
        rewriteProps ps [] =
          -- delete all properties mentioned in LHS but not in RHS
          fmap (\(Prop(k, _)) -> RemProp k) ps
        -- add properties mentioned only in RHS
        rewriteProps [] ps' = fmap SetProp ps'
        rewriteProps (p@(Prop(k,_)):ps) (p'@(Prop(k',_)):ps') =
          case compare k k' of
            -- drop property mentioned in LHS but not in RHS
            LT -> RemProp k : rewriteProps ps (p':ps')
            -- reset property mentioned in both LHS and RHS
            EQ -> SetProp p' : rewriteProps ps ps'
            -- set property mentioned only in RHS
            GT -> SetProp p' : rewriteProps (p:ps) ps'

references :: [Pattern] -> [Reference]
references = (\(_, _, rs) -> reverse rs) . foldl' go (0, mempty :: Set Name, [])
  where
    go = bifoldl' node edge
      where
        node (anonId, seen, refs) (NodeSpec Nothing _ _) =
          (anonId + 1, seen, Anon anonId : refs)
        node (anonId, seen, refs) (NodeSpec (Just var) _ _) =
          (anonId, S.insert var seen, Named var : refs)
        edge (anonId, seen, refs) (EdgeSpec Nothing _ _ _) =
          (anonId + 1, seen, Anon anonId : refs)
        edge (anonId, seen, refs) (EdgeSpec (Just var) _ _ _) =
          (anonId, S.insert var seen, Named var : refs)
