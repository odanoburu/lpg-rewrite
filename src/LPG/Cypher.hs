module LPG.Cypher (cypher) where

import Data.Bifoldable
import Data.List (foldl')
import qualified Data.Map as M
import Data.Maybe (fromMaybe)
import Data.Set (Set)
import qualified Data.Set as Set
import Data.String (IsString)
import Data.Text.Lazy (Text)
import Data.Text.Lazy.Builder (toLazyText, fromString)

import LPG.Data ( EdgeDir(..), EdgeSpec(..), Label(..), Name, NodeSpec(..)
                , Prop(..), Reference(..), RTree(..), Rule(..))
import LPG.ChangeSet (ChangeSet, Rewrite(..), references)

idMapName :: IsString a => a
idMapName = "ids"

cypher :: Rule -> ChangeSet -> Text
cypher Rule{lhs, rhs, ret} changes = toLazyText $
  let (_vars, matches) = foldl' goMatches (mempty :: Set Name, "") lhs
  in matches <> changesSubQuery <> ret'
  where
    showPattern = bifoldMap node edge
      where
        node (NodeSpec mname ls prps) =
          mconcat ["(", spec mname ls prps, ")"]
        edge (EdgeSpec Nothing L [] []) = "<--"
        edge (EdgeSpec Nothing R [] []) = "-->"
        edge (EdgeSpec mname dir ls prps) =
          mconcat [leftArr, "[", spec mname ls prps, "]", rightArr]
          where
            (leftArr, rightArr) = case dir of L -> ("<-", "-"); R -> ("-", "->")
        spec mname ls prps =
          mconcat [maybe "" fromString mname, labels ls, props prps]
          where
            labels = foldr go ""
              where
                go (Label l) r = ":" <> fromString l <> r
            props [] = ""
            props ps = mconcat [" {", commas ps', "}"]
              where
                ps' = fmap go ps
                  where
                    go (Prop (k, v)) =
                      mconcat [fromString k, ":", fromString $ show v]
    goMatches (vars, q) p =
      (vars', mconcat [q, "MATCH ", showPattern p, "\n", wheres . Set.toList $ newVars Set.\\ vars, "\n", withs vars'])
      where
        vars' = vars <> newVars
        newVars = bifoldMap nodeVars edgeVars p
          where
            nodeVars (NodeSpec (Just name) _ _) = Set.singleton name
            nodeVars _ = mempty
            edgeVars (EdgeSpec (Just name) _ _ _) = Set.singleton name
            edgeVars _ = mempty
        wheres [] = ""
        wheres introducedVars = mconcat ["WHERE ", list " AND " (fmap goWhere introducedVars)]
          where
            goWhere var = mconcat ["(", idParam, " IS NULL OR id(", v, ") = ", idParam, ")"]
              where v = fromString var
                    idParam = mconcat ["$", idMapName, ".", v]
    withs vars = go $ Set.toList vars
      where
        go [] = ""
        go vs = mconcat ["WITH ", commas (fmap fromString vs), "\n"]
    changesSubQuery =
      let (remainingChanges, referencedChanges) = foldl' goRewrites (changes, "") refs
          unreferencedChanges = M.foldlWithKey' rewrites "" remainingChanges
      in referencedChanges <> unreferencedChanges
      where
        refs = references $ fromMaybe [] rhs
        goRewrites (remChanges, query) ref =
          case M.updateLookupWithKey (\ _ _ -> Nothing) ref remChanges of
            (Nothing, remChanges') -> (remChanges', query)
            (Just rws, remChanges') -> (remChanges', rewrites query ref rws)
        rewrites query ref =
          foldl' (\q rw -> q <> "\n" <> rewrite ref rw) query
        rewrite ref rw = go
          where
            mname = case ref of
                      Named name -> Just name
                      Anon{} -> Nothing
            var' =
              fromMaybe (error "Anonymous node does not support rewrite operation")
              mname
            var = fromString var'
            createNodeQuery ls prps = "CREATE "
              <> showPattern (Leaf $ NodeSpec mname ls prps)
            createEdgeQuery src tgt ls prps = "CREATE "
              <> showPattern (Node (NodeSpec (Just src) [] [])
                              (EdgeSpec mname R ls prps)
                              (Leaf $ NodeSpec (Just tgt) [] []))
            go =
              case rw of
                DeleteNode -> "DELETE " <> var
                DeleteEdge -> "DELETE " <> var
                AddNode ls prps ->
                  createNodeQuery ls prps
                AddEdge src tgt ls prps ->
                  createEdgeQuery src tgt ls prps
                ChangeEdge src tgt ls prps ->
                  mconcat ["DELETE ", var, " ", createEdgeQuery src tgt ls prps]
                AddLabel (Label l) ->
                  mconcat ["SET ", var, ":", fromString l]
                RemLabel (Label l) ->
                  mconcat ["REMOVE ", var, ":", fromString l]
                SetProp (Prop (k, v)) ->
                  mconcat ["SET ", var, ".", fromString k, " = ", fromString $ show v]
                RemProp k ->
                  mconcat ["REMOVE ", var, ".", fromString k]
    list sep = foldr1 (\l r -> l <> sep <> r)
    commas = list ", "
    ret' = case ret of
      [] -> ""
      _  -> mconcat [ "\n\nRETURN "
                    , commas (fmap (fromString.show) ret)]
