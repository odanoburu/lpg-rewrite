module LPG.Data (Description, EntityDesc(..), EdgeDir(..), EdgeSpec(..), Expr(..)
                , Label(..), Name, NodeSpec(..), Pattern, Prop(..)
                , Reference(..), Rule(..), RTree(..), Str, Value(..), addEntityDesc
                , list, maybeNonEmpty, nonEmptyMap, singleton) where

import Data.Bifoldable (Bifoldable(..))
import Data.List.NonEmpty (NonEmpty(..))
import Data.Map (Map)
import qualified Data.Map.Strict as M
import Data.String(IsString(..))

type Str = String
type Name = String
data Value = B Bool | I Int | T Str
  deriving (Eq, Ord)
instance Show Value where
  show (B True) = "true"
  show (B False) = "false"
  show (I i) = show i
  show (T str) = show str

data Expr
  = V Value
  | Lookup Name Name -- very limited form of lookups for now
  | Li [Expr] -- lists
  | FC Name Name -- very limited function calls varname(varname)
  | M [Prop]
  deriving (Eq, Ord)
instance Show Expr where
  show (Lookup thing what) = concat [thing, ".", what]
  show (V v) = show v
  show (Li es) = show es
  show (M []) = "{}"
  show (M kvs) = concat ["{"
                        , foldr1 (\a b -> concat [a, ", ", b])
                          $ fmap (\(Prop (k, e)) -> concat [k, ": ", show e]) kvs
                        , "}"]
  show (FC fname var) = concat [fname, "(", var, ")"]

newtype Label = Label Name
  deriving (Eq, Ord, Show)
instance IsString Label where
  fromString = Label

newtype Prop  = Prop (Name, Expr)
  deriving (Eq, Ord, Show)
data EdgeDir = R | L
  deriving (Eq, Show)
data RTree a b = Leaf a | Node a b (RTree a b)
  deriving (Eq, Show)

instance Bifoldable RTree where
  -- bifoldMap :: Monoid m => (a -> m) -> (b -> m) -> p a b -> m
  bifoldMap f _ (Leaf x) = f x
  bifoldMap f g (Node x y t) = f x <> g y <> bifoldMap f g t

data NodeSpec = NodeSpec (Maybe Name) [Label] [Prop]
  deriving (Eq, Show)
data EdgeSpec = EdgeSpec (Maybe Name) EdgeDir [Label] [Prop]
  deriving (Eq, Show)
type Pattern = RTree NodeSpec EdgeSpec


data Rule =
  Rule { lhs :: NonEmpty Pattern
       , rhs :: Maybe [Pattern] -- Nothing : no rhs, Just []: empty rhs (delete
                                -- every named thing mentioned in lhs)
       , ret :: [Expr]
       }
  deriving (Eq, Show)

maybeNonEmpty :: b -> (NonEmpty a -> b) -> Maybe [a] -> b
maybeNonEmpty _ f (Just (x:xs)) = f (x:|xs)
maybeNonEmpty def _ _ = def

data EntityDesc
  = NodeDesc [Label] [Prop]
  | EdgeDesc Name Name [Label] [Prop]
  deriving (Eq, Show)
data Reference = Named Name | Anon Word deriving (Eq, Ord, Show)
type Description = Map Reference EntityDesc

addEntityDesc :: Reference -> EntityDesc -> Description -> Description
addEntityDesc = M.insert

singleton :: a -> NonEmpty a
singleton x = x :| []

nonEmptyMap :: b -> (NonEmpty a -> b) -> [a] -> b
nonEmptyMap def _ [] = def
nonEmptyMap _ f (x:xs) = f (x:|xs)

list :: b -> (a -> [a] -> b) -> [a] -> b
list def _ [] = def
list _ f (x:xs) = f x xs
