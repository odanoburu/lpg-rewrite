module LPG.Parser (Parser, Str, parseRule, parseRuleSet, patternP, rule, ruleSet
                  , spaceConsumer) where

import LPG.Data ( EdgeDir(..), EdgeSpec(..), Expr(..), Label(..), Name
                , NodeSpec(..), Pattern, Prop(..), Rule(..), RTree(..), Str, Value(..))

import qualified Control.Applicative.Combinators.NonEmpty as C
import Data.Bifunctor (bimap)
import Data.Char (isSpace)
import Data.Functor (($>))
import Data.List (sort)
import Data.List.NonEmpty (NonEmpty)
import Data.Void (Void)
import Prelude hiding (pred)
import Text.Megaparsec hiding (Label)
import Text.Megaparsec.Char (alphaNumChar, char, letterChar, newline)
import qualified Text.Megaparsec.Char.Lexer as L


type Parser = Parsec Void Str


rule :: Parser Rule
rule = Rule <$> C.some (patternP id) <*> optional rhsP <*> returnP
  where
    rhsP = symbol "=>" *> many (patternP id)


patternP :: (Pattern -> Pattern) -> Parser Pattern
patternP mp = do
  n <- node
  me <- optional edge
  case me of
    Nothing -> return . mp $ Leaf n
    Just e -> patternP (mp . e n)
  where
    node = parens (named <|> anon)
      where
        anon = NodeSpec Nothing <$> labels <*> properties
        named = NodeSpec
          <$> (Just <$> identifier)
          <*> labels <*> properties
    edge = rightEdge <|> leftEdge
      where
        rightEdge = symbol "-" *> edgeDir R <* symbol "->"
        leftEdge = symbol "<-" *> edgeDir L <* symbol "-"
        edgeDir d
          = brackets (named <|> (anon <$> labels <*> properties))
          <|> pure (anon [] [])
          where
            named = (\n ls prps rnode p -> Node rnode (EdgeSpec n d ls prps) p)
              <$> (Just <$> identifier) <*> labels <*> properties
            anon ls prps rnode = Node rnode (EdgeSpec Nothing d ls prps)


returnP :: Parser [Expr]
returnP = expression `sepBy` comma


sortNoDuplicates :: Ord a => String -> (a -> a -> Bool) -> Parser [a] -> Parser [a]
sortNoDuplicates what pred p = do
  mxs <- go <$> p
  case mxs of
    Just xs -> return xs
    Nothing -> fail $ "No repeat " ++ what ++ " are allowed"
  where
    go xs = if noDuplicates xs' then Just xs' else Nothing
      where
        xs' = sort xs
        noDuplicates [] = True
        noDuplicates [_] = True
        noDuplicates (x:y:_)
          | pred x y = False
        noDuplicates (_:y:ys) = noDuplicates (y:ys)


labels :: Parser [Label]
labels = sortNoDuplicates "labels" (==) $ many (colon *> (Label <$> identifier))

properties :: Parser [Prop]
-- NOTE: does not fail
properties = option [] properties'

properties' :: Parser [Prop]
-- NOTE: will parse {}
properties' = braces . sortNoDuplicates "property names" sameName
  $ sepEndBy1 property comma
  where
    property = curry Prop <$> (identifier <* colon) <*> expression
    sameName (Prop(n,_)) (Prop(n',_)) = n == n'


expression :: Parser Expr
expression = V <$> try value
  <|> Lookup <$> try (identifier <* symbol ".") <*> identifier
  <|> Li <$> brackets (sepEndBy expression comma)
  <|> M <$> properties'
  <|> FC <$> (identifier <* symbol "(") <*> identifier <* symbol ")"


value :: Parser Value
value = B <$> bool <|> I <$> int <|> T <$> stringLiteral
  where
    bool = (symbol "true" $> True) <|> (symbol "false" $> False)
    int = lexeme L.decimal
    stringLiteral = char '"' >> manyTill L.charLiteral (char '"')


identifier :: Parser Str
identifier = lexeme ((:) <$> letterChar <*> many alphaNumChar)


parens, braces, brackets :: Parser a -> Parser a
parens = between (symbol "(") (symbol ")")
braces = between (symbol "{") (symbol "}")
brackets = between (symbol "[") (symbol "]")


colon, comma :: Parser ()
colon = symbol ":" $> ()
comma = symbol "," $> ()


spaceConsumer :: ParsecT Void String m ()
spaceConsumer = L.space spaces lineComment blockComment
  where
    lineComment = L.skipLineComment "//"
    blockComment = L.skipBlockComment "/*" "*/"
    spaces = do
      _ <- takeWhile1P Nothing isSpace
      return ()


lexeme :: Parser a -> Parser a
lexeme = L.lexeme spaceConsumer


symbol :: String -> Parser String
symbol = L.symbol spaceConsumer


parseInput :: Parser a -> Str -> Either String a
parseInput p input = bimap errorBundlePretty id
  $ parse go "" input
  where
    go = spaceConsumer *> p <* eof

parseRule :: Str -> Either String Rule
parseRule = parseInput rule

parseRuleSet :: Str -> Either String (NonEmpty (Name, Rule))
parseRuleSet = parseInput ruleSet

ruleSet :: Parser (NonEmpty (Name, Rule))
ruleSet = C.some namedRule
  where
    namedRule = (,) <$> (symbol "#" *> lexeme (someTill anySingle newline))
      <*> rule
