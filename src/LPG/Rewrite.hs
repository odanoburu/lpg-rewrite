{- |
Copyright: (c) 2020 bruno cuconato
SPDX-License-Identifier: BSD-3-Clause
Maintainer: bruno cuconato <bcclaro+haskell@gmail.com>

See README for more info
-}

module LPG.Rewrite
       ( changeSet
       , cypher
       , cypher'
       , parseRule
       ) where

import Data.Bifunctor (bimap)
import Data.List.NonEmpty (NonEmpty(..))
import qualified Data.Map as M
import Data.Text.Lazy (Text)

import LPG.Data (Reference)
import LPG.Parser (Str, parseRule)
import qualified LPG.ChangeSet as CS
import qualified LPG.Cypher as CY

changeSet :: Str -> Either (NonEmpty String) [(Reference, NonEmpty CS.Rewrite)]
changeSet input
  = M.toList <$>
  (bimap (:|[]) id (parseRule input)
   >>= CS.changeSet)

cypher :: Str -> Either (NonEmpty String) Text
cypher input = do
  rule <- bimap (:|[]) id (parseRule input)
  changes <- CS.changeSet rule
  return $ CY.cypher rule changes

cypher' :: Str -> Text
cypher' input =
  let eCypher = cypher input
  in case eCypher of
       Right query -> query
       Left (e:|es) -> error $ unlines (e:es)
