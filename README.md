# lpg-rewrite

[![GitHub CI](https://github.com/odanoburu/lpg-rewrite/workflows/CI/badge.svg)](https://github.com/odanoburu/lpg-rewrite/actions)
[![Hackage](https://img.shields.io/hackage/v/lpg-rewrite.svg?logo=haskell)](https://hackage.haskell.org/package/lpg-rewrite)
[![Stackage Lts](http://stackage.org/package/lpg-rewrite/badge/lts)](http://stackage.org/lts/package/lpg-rewrite)
[![Stackage Nightly](http://stackage.org/package/lpg-rewrite/badge/nightly)](http://stackage.org/nightly/package/lpg-rewrite)
[![BSD-3-Clause license](https://img.shields.io/badge/license-BSD--3--Clause-blue.svg)](LICENSE)

See README for more info
