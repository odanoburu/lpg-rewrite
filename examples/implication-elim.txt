//# implication elimination
(prev:Rule)-[m:DerivedBy]->(goal:Goal)
(goal)-[r:Derives]->(ded:Formula)
(ant:Formula)-[:Builds {ant: true}]->(imp:Formula)<-[:Builds {csq: true}]-(ded)

=>

(prev)-[:DerivedBy {name: m.name}]->(n:Rule:ImpElim)-[:Derives]->(ded)
(n)-[:DerivedBy]->(goal)-[:Derives]->(ant)
(n)-[:DerivedBy]->(goal2)-[:Derives]->(imp)

{ruleName: "impElim", ruleId: id(n), ruleDed: id(ded)}, [{goalRole: "etor", goalId: id(goal), goalDed: id(ant)}, {goalRole: "eted", goalId: id(goal2), goalDed: id(imp)}]
